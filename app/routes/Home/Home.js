import React from 'react';
import { Component, StyleSheet, Text, View, ListView, TouchableHighlight } from 'react-native';
import { COLORS } from '../../styles';
import Button from '../../components/Button';
import Meteor, { createContainer, MeteorListView } from 'react-native-meteor';
import Icon from 'react-native-vector-icons/FontAwesome';
import Loading from '../../components/Loading.js';
import Routes from '../';

const ArrowIcon = (<Icon name="arrow-right" size={30} color="#900" />);
const onDetailsPress = (navigator) => {
  return navigator.push(Routes.getDetailsRoute());
};

class Home extends Component {

  constructor(props) {
    super(props);

    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    });

    this.state = {
      driverTrips: this.props.driverTrips,
      driverTripsReady: this.props.driverTripsReady,
      dataSource: this.ds.cloneWithRows([]),
    };
  }

  renderTrips(trip) {
    return (
      <TouchableHighlight onPress={() => onDetailsPress(this.props.navigator)}>
        <View style={Styles.tripContainer}>
          <View style={Styles.tripDetails}>
            <Text>Trip# {trip.tripNumber}  </Text>
            <Text>{trip.puName}</Text>
            <Text>{trip.puCity}, {trip.puState}</Text>
          </View>
          <View style={Styles.arrow}>
            <Icon name="arrow-right" size={30} color="#81c04d" />
          </View>
          <View style={Styles.tripDetails}>
            <Text>Trl# {trip.trailerNum}</Text>
            <Text>{trip.delName}</Text>
            <Text>{trip.delCity}, {trip.delState}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }

  render() {
    if (this.props.user.profile.userType !== 'driver') {
      return (
        <View>
          <Text>
            Sorry but office staff do not have login privilages to the drivers app.
          </Text>
        </View>
      );
    }

    if (!this.props.driverTripsReady) {
      return (
        <View>
          <Loading />
        </View>
      );

    }

    console.log(this.props.driverTrips.length);

    if (this.props.driverTrips.length > 0) {

      return (
        <View>
          <ListView
            dataSource={this.ds.cloneWithRows(this.props.driverTrips[0].dispatchedStops)}
            renderRow={this.renderTrips.bind(this)} />
        </View>
      );
    }

    if (this.props.driverTrips.length === 0) {
      return (
        <View>
          <Text>
            No Trips Currently Dispatched .....
          </Text>
        </View>
      );
    }
  };
};

Home.propTypes = {
  navigator: React.PropTypes.object,
};

export default createContainer(params=> {

  const handle = Meteor.subscribe('driversBoard', 'khalistransport', 'subscribeDriversApp', 155);

  return {
    driverTripsReady: handle.ready(),
    driverTrips: Meteor.collection('driversBoard').find({}),
    user: Meteor.user(),
  };
}, Home);

const Styles = StyleSheet.create({
  tripContainer: {
        backgroundColor: '#F5FCFF',
        paddingTop: 30,
        paddingBottom: 10,
        flexDirection: 'row',
        flex: 1,    //Step 1
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
      },
  tripDetails: {
      flex: 40,            //Step 2
    },
  arrow: {
      flex: 20,
      justifyContent: 'space-around',
      //Step 3
    },
});
